// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:homemed_mobile/detailproduct/screens/detailpage/detail_page.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class catalog extends StatelessWidget {
  const catalog({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override 
  Widget build(BuildContext context) {
    Future<void> fetchData() async {
    const url = 'http://homemed2.herokuapp.com/catalog/json';
    try {
      final response = await http.get(Uri.parse(url));
      print(response.body);
      Map<String, dynamic> extractedData = jsonDecode(response.body);
      extractedData.forEach((key, val) {
        print(val);
      });
    } catch (error) {
      print(error);
    }
  }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: const Color(0xFFFFFFFF),
        fontFamily: 'Georgia',
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 5.0),
        ),
      ),
      home: Scaffold(
        appBar: AppBar(title: Text("Catalog"),),
        body: Column(children: <Widget>[
        Row(
          children: <Widget>[
            Column(children: <Widget>[
              Container(
              height: 200,
              width: 180,
              
              decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16),
              ),
              child: FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                      MaterialPageRoute(
                        builder: (context) {
                          return detailPage();
                          },
                        ),
                      );
                },
                child: Image.asset('assets/images/kn95fix.jpg',),
              ),
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 5.0),
            child: Text(
              "Masker KN95",
              ),
            ),
            ],
            ),

            Column(children: <Widget>[
              Container(
                height: 200,
                width: 180,
                decoration: BoxDecoration(
                // color: Theme.of(context).colorScheme.secondary,
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Image.asset('assets/images/gloves.png'),
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 5.0),
            child: Text("Sarung Tangan"),
            ),
            ],
            ),
           Column(children: <Widget>[
              Container(
              height: 200,
              width: 180,
              
              decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16),
              ),
              child: Image.asset('assets/images/hearingaid.png'),
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 5.0),
            child: Text(
              "Hearing Aid",
              ),
            ),
            ],
            ),            
          ],
        ),
        Row(children: const <Widget>[
          Center(child: Text("Berikan saran barang yang Anda cari:",
              textAlign: TextAlign.center,),)
              
              
            ],),
        // Row(children: [
        //   Form(
        //     child: ListView(
        //       padding: EdgeInsets.all(16),
        //       children: [
        //         saran(),
        //         const SizedBox(height: 16),
        //       ],))],)        
        ],)
      ),
      
    );
    
  }
  // Widget saran() => TextFormField(
  //     decoration: InputDecoration(
  //       labelText: 'Saran',
  //       border: OutlineInputBorder(),
  //     ),
  //     validator: (value) {
  //       if (value!.isEmpty) {
  //         return 'Beri Saran';
  //       }
  //     },
  //   );
}
