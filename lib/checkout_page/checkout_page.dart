import 'package:flutter/material.dart';

import 'widgets/currency_format.dart';
import 'widgets/item_product.dart';
import 'package:http/http.dart' as http;

import 'dart:convert';

class CheckoutPage extends StatefulWidget {
  const CheckoutPage({Key? key}) : super(key: key);

  @override
  State<CheckoutPage> createState() => _CheckoutPageState();
}



class _CheckoutPageState extends State<CheckoutPage> {
  late int jumlah = 0;
  late List<dynamic> _decodeIsiprod;
  late List<dynamic> _decodeIsiKeranjang;
  late List<ItemProduct> inside = [];
   // ignore: non_constant_identifier_names
   Future<void> delete(int id) async{
    String checkout = 'https://homemed2.herokuapp.com/cart/delete_flutter/' + id.toString() ;
    try{
      final tc = await http.post(
        Uri.parse(checkout),
        headers: <String, String>{'Content-type' : 'application/json;'},
      );
      // ignore: avoid_ print
      print(tc.body);
      _decodeIsiprod = await jsonDecode(tc.body);
    }
    catch(error){
      // ignore: avoid_print
      print(error);
    }
  }
  Future<void> checkout() async{
    String checkout = 'https://homemed2.herokuapp.com/cart/checkout_flutter';
    try{
      final tc = await http.get(
        Uri.parse(checkout),
        headers: <String, String>{'Content-type' : 'application/json;'},
      );
      // ignore: avoid_print
      print(tc.body);
      _decodeIsiprod = await jsonDecode(tc.body);
    }
    catch(error){
      // ignore: avoid_print
      print(error);
    }
  }

   Future<void> update_kurang(int id) async{
    String update_kurang_barang = 'https://homemed2.herokuapp.com/cart/cart_list_update_kurang/' + id.toString();
    try{
      final tc = await http.post(
        Uri.parse(update_kurang_barang),
        headers: <String, String>{'Content-type' : 'application/json;'},
      );
      // ignore: avoid_print
      print(tc.body);
      _decodeIsiprod = await jsonDecode(tc.body);
    }
    catch(error){
      // ignore: avoid_print
      print(error);
    }
  }
  // ignore: non_constant_identifier_names
  Future<void> update_tambah(int id) async{
    String update_tambah_barang = 'https://homemed2.herokuapp.com/cart/cart_list_update_tambah/' + id.toString();
    try{
      final tc = await http.post(
        Uri.parse(update_tambah_barang),
        headers: <String, String>{'Content-type' : 'application/json;'},
      );
      // ignore: avoid_print
      print(tc.body);
      _decodeIsiprod = await jsonDecode(tc.body);
    }
    catch(error){
      // ignore: avoid_print
      print(error);
    }
  }


  Future<void> fetchData() async{
    int temp = 0;
    String urlkeranjang = 'https://homemed2.herokuapp.com/cart/cart_list_flutter';
    try{
      final isiKeranjang = await http.get(
        Uri.parse(urlkeranjang),
        headers: <String, String>{'Content-type' : 'application/json;'},
      );
      // ignore: avoid_print
      print(isiKeranjang.body);
      _decodeIsiprod = await jsonDecode(isiKeranjang.body);
    }
    catch(error){
      // ignore: avoid_print
      print(error);
    }
    String urlkeran = 'https://homemed2.herokuapp.com/cart/cart_list_flutter_isi';
    try{
      final isiKeran = await http.get(
        Uri.parse(urlkeran),
        headers: <String, String>{'Content-type' : 'application/json;'},
      );
      // ignore: avoid_print
      print(isiKeran.body);
      _decodeIsiKeranjang = await jsonDecode(isiKeran.body);
      
      
    }
    catch(error){
      print(error);
    }
    _decodeIsiKeranjang.forEach((element_keranjang) {
      _decodeIsiprod.forEach((element_produk) {
        if (element_keranjang["fields"]['Barang'] == element_produk["pk"]) {
          inside.add(ItemProduct(title: element_produk["fields"]["name"], price: element_produk["fields"]["price"] , 
          assetPath: element_produk["fields"]["image"], jumlah: element_keranjang["fields"]["jumlah"], id: element_produk["pk"]));
        }
      });
    });
    inside.forEach((elementsini) { 
      temp+=(elementsini.jumlah * elementsini.price);
    });
    ItemProduct.jumlahKeseluruhan = temp;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return FutureBuilder(
      future: fetchData(),
      builder: (BuildContext context, AsyncSnapshot<void> snapshot){
        if(snapshot.connectionState == ConnectionState.waiting){
          return Center(child : Text("Please wait its loading..."));
        }else{
          return Scaffold(
      
      appBar: AppBar(
        leading: const BackButton(
          color: Colors.greenAccent,
        ),
        title: const Text(
          "Keranjang",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Container(
        height: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.blue,
              Colors.greenAccent,
            ],
          ),
        ),
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16),
          
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [ListView.builder(
              itemCount: inside.length,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (context,index){
                return Container(
      margin: const EdgeInsets.symmetric(vertical: 16),
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            inside[index].assetPath,
            width: 100,
          ),
          const SizedBox(width: 22),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  inside[index].title,
                  style: textTheme.headline6,
                ),
                const SizedBox(height: 16),
                Text(
                  "Rp. " + inside[index].price.toString(),
                  style: textTheme.subtitle1!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 16),
                Container(
                  
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: IconButton(
                          onPressed: (){
                            if(inside[index].jumlah > 0){
                              setState(() {  
                                  update_kurang(inside[index].id);
                                  inside = [];
                                  
                              });
                            }
                          },
                          icon: const Icon(Icons.minimize, size: 16,),
                        ),
                      ),
                      const SizedBox(width: 22),
                      Text(inside[index].jumlah.toString()),
                      const SizedBox(width: 22),
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: IconButton(
                          onPressed: (){
                            if(inside[index].jumlah >= 0){
                              
                              // ignore: avoid_print
                              setState(() {
                               
                                update_tambah(inside[index].id);
                                inside = [];
                              });
          
                            }
                          },
                          icon: const Icon(Icons.add, size: 16,),
                        ),
                        
                      ),
                      const SizedBox(width: 22),
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          color: Colors.red
                        ),
                        child: IconButton(
                          onPressed: (){
                            if(inside[index].jumlah >= 0){
                              
                              // ignore: avoid_print
                              setState(() {
                               
                                delete(inside[index].id);
                                inside = [];
                              });
          
                            }
                          },
                          icon: const Icon(Icons.cancel, size: 20 ,)
                        ),
                        
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 8),
              ],
            ),
          ),
        ],
      ),
    );
              }
              
              ),
              const SizedBox(height: 250),
              Text(
                "Total: " + CurrencyFormat.convertToIdr(ItemProduct.jumlahKeseluruhan, 2),
                style:
                    textTheme.headline5!.copyWith(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    primary: Colors.black,
                  ),
                  onPressed: () {
                    setState(() {
                      checkout();
                      inside = [];
                    });
                  },
                  child: Text(
                    "Checkout",
                    style: textTheme.headline6!.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
        }
      }
    );
  }
}
