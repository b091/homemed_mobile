import 'package:flutter/material.dart';

class ItemProduct extends StatelessWidget {
 
  ItemProduct({
    Key? key,
    required this.title,
    required this.price,
    required this.assetPath,
    required this.jumlah,
    required this.id,
    
  }) : super(key: key);
  static int jumlahKeseluruhan =0;
  final String title;
  final String assetPath;
  final int price;
  int  jumlah;
  int id;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16),
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            assetPath,
            width: 100,
          ),
          const SizedBox(width: 22),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: textTheme.headline6,
                ),
                const SizedBox(height: 16),
                Text(
                  "Rp. $price",
                  style: textTheme.subtitle1!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 16),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: IconButton(
                          onPressed: (){

                          },
                          icon: const Icon(Icons.minimize, size: 16,),
                        ),
                      ),
                      const SizedBox(width: 22),
                      Text(jumlah.toString()),
                      const SizedBox(width: 22),
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: IconButton(
                          onPressed: (){

                          },
                          icon: const Icon(Icons.add, size: 16,),
                        ),
                      ),
                      
                    ],
                  ),
                ),
                const SizedBox(height: 8),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
