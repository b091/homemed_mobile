import 'package:flutter/material.dart';

class about extends StatefulWidget {
  @override
  _aboutState createState() => _aboutState();
}

class _aboutState extends State<about> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: [
            Positioned(
                child: Container(
              width: double.maxFinite,
              height: 300,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("images/health-topics-2.jpeg"),
                      fit: BoxFit.cover)),
            )),
            Positioned(
                left: 20,
                top: 40,
                child: Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back),
                      color: Colors.cyan,
                    )
                  ],
                )),
            Positioned(
                top: 240,
                child: Container(
                  padding: const EdgeInsets.only(left: 30, right: 20, top: 20),
                  width: MediaQuery.of(context).size.width,
                  height: 500,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            "HomeMed",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.cyan,
                              fontSize: 35,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce quis pulvinar eros. Cras tincidunt risus tortor, eu lobortis sapien dignissim at. Nullam vitae facilisis mi, eget tempus sem. Sed luctus augue ac accumsan sodales. Sed convallis est est, ac fermentum enim commodo eu. Quisque vel lectus in libero commodo elementum nec vitae nisl. Morbi sollicitudin gravida metus sed faucibus. Aenean sed sollicitudin est. Nunc ullamcorper et ipsum in feugiat.",
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.black,
                          fontSize: 15,
                        ),
                      )
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
