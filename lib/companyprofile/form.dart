import 'package:flutter/material.dart';

class form extends StatefulWidget {
  @override
  _formState createState() => _formState();
}

class _formState extends State<form> {
  final _formKey = GlobalKey<FormState>();
  double nilaiSlider = 1;
  bool checkbox1 = false;
  bool nilaiSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Text("Form Kerja Sama"),
        flexibleSpace: SafeArea(
          child: Container(
            padding: const EdgeInsets.only(right: 16),
            child: Row(children: <Widget>[]),
          ),
        ),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(9.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: something@gmail.com",
                      labelText: "Masukan email",
                      icon: Icon(Icons.email),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Email tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    maxLines: null,
                    decoration: new InputDecoration(
                      labelText:
                          "Masukan alasan anda ingin bekerja sama dengan kami",
                      icon: Icon(Icons.move_to_inbox),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Alasan tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                // Row(children: [
                //   SizedBox(
                //     width: 10,
                //     child: Checkbox(
                //       value: checkbox1,
                //       activeColor: Colors.orange,
                //       onChanged: (value) {
                //         //value may be true or false
                //         setState(() {
                //           checkbox1 = !checkbox1;
                //         });
                //       },
                //     ),
                //   ),
                //   SizedBox(width: 10.0),
                //   Text('I accept the Term and Conditions')
                // ]),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    final isValid = _formKey.currentState!.validate();

                    if (isValid) {
                      _formKey.currentState!.save();

                      final message = 'Form berhasil dikirimkan';

                      final snackBar = SnackBar(
                        content: Text(
                          message,
                          style: TextStyle(fontSize: 16),
                        ),
                        backgroundColor: Colors.green,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
