import 'package:flutter/material.dart';
import 'package:homemed_mobile/companyprofile/about.dart';
import 'package:homemed_mobile/companyprofile/form.dart';
import 'package:homemed_mobile/companyprofile/quote.dart';

class cp extends StatelessWidget {
  static String title = 'Company Profile';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primarySwatch: Colors.green),
        home: cpPage(title: title),
      );
}

class cpPage extends StatefulWidget {
  final String title;

  const cpPage({
    required this.title,
  });

  @override
  cpState createState() => cpState();
}

class cpState extends State<cpPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Text(widget.title),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(15),
            // ignore: deprecated_member_use
            child: RaisedButton.icon(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => about()));
              },
              label: Text('About Company'),
              icon: Icon(
                Icons.business_center,
                color: Colors.cyan,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            // ignore: deprecated_member_use
            child: RaisedButton.icon(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => form()));
              },
              label: Text('Form Kerja Sama'),
              icon: Icon(
                Icons.forward_to_inbox,
                color: Colors.cyan,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(15),
            // ignore: deprecated_member_use
            child: RaisedButton.icon(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => quote()));
              },
              label: Text('Team'),
              icon: Icon(
                Icons.emoji_people,
                color: Colors.cyan,
              ),
            ),
          )
        ],
      )
          // ignore: deprecated_member_use
          ),
    );
  }
}
