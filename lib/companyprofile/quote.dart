import 'package:flutter/material.dart';

class quote extends StatefulWidget {
  @override
  _quoteState createState() => _quoteState();
}

class _quoteState extends State<quote> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.cyan,
          title: Text("Meet Our Team"),
          flexibleSpace: SafeArea(
            child: Container(
              padding: const EdgeInsets.only(right: 16),
              child: Row(children: <Widget>[]),
            ),
          ),
        ),
        body: Center(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.cyan,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                )),
            child: Text(
              '2006597323-Aurora Putri Kumala Bakti \n2006597531-Nathasya Shalsabilla Putri \n2006597374-Brasel Isnaen Fachturrahman \n2006597361-Christopher Moses Nathanael \n2006597494-Muhammad Arya Adirianto \n2006597121-Galan Gahara Triguna Toding \n2006597670-Dimitri Theofilus Wynardo N',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
            alignment: Alignment.center,
            constraints: BoxConstraints.tightForFinite(
              width: 300,
              height: 300,
            ),
          ),
        ));
  }
}
