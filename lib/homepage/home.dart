// ignore_for_file: prefer_const_constructors

//referensi diambil dari https://github.com/JohannesMilke/card_example

import 'package:flutter/material.dart';
import 'package:homemed_mobile/cs/custsup.dart';
import 'package:homemed_mobile/companyprofile/company.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

// ignore: use_key_in_widget_constructors
class homePage extends StatelessWidget {
  static String title = 'Homemed Homepage';

    Future<void> fetchData() async{
    int temp = 0;
    String urlHome = 'http://127.0.0.1:8000/json';
    try {
      final isiHome = await http.get(
        Uri.parse(urlHome),
        headers: <String, String>{'Content-type' : 'application/json'},
      );
      print(isiHome);
    } catch (e) {
    }
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primarySwatch: Colors.cyan),
        home: MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _MainPageStateIndex = 0;
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: TextField(
             onTap: () {},
            readOnly: true,
            style: TextStyle(fontSize: 15),
            decoration: InputDecoration(
                hintText: 'Search',
                prefixIcon: Icon(Icons.search, color: Colors.orange),
                contentPadding: const EdgeInsets.symmetric(vertical: 10.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: new BorderSide(color: Colors.white),
                ),
                fillColor: Color(0xfff3f3f4),
                filled: true),
          ),
        ),
        body: ListView(
          padding: EdgeInsets.all(16),
          children: [
            Container(
              child: productDummy1(),
              margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
            ),
            Container(
              child: productDummy2(),
              margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
            ),
            Container(
              child: productDummy3(),
              margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
            ),
            Container(
              child: Text('Join Us',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  )),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
            ),
            TextField(
              decoration: const InputDecoration(
                  border: UnderlineInputBorder(), hintText: 'Enter your Email'),
            ),
            Positioned(
                child: ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16.0),
                              side: BorderSide(
                                color: Colors.cyan,
                              )))),
                  child: Text('Submit'),
                  onPressed: () {
                    homePage().fetchData();
                  },
                ),
              ],
            )),
          ],
        ),

      );

  Widget productDummy1() => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Ink.image(
                  image: NetworkImage(
                    'https://m.media-amazon.com/images/I/61+-lxwiE6S._AC_SL1000_.jpg',
                  ),
                  height: 240,
                  fit: BoxFit.contain,
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  left: 16,
                  child: Text(
                    'ZyeZoo Disposable Face Mask : Rp.70.000',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 24,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
                child: ButtonBar(
              alignment: MainAxisAlignment.start,
            )),
          ],
        ),
      );

  Widget productDummy2() => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Ink.image(
                  image: NetworkImage(
                    'https://cf.shopee.co.id/file/6f956b518c8ed0bef25915559cc1839c',
                  ),
                  height: 240,
                  fit: BoxFit.contain,
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  left: 16,
                  child: Text(
                    'Kursi Roda Stainless Steel One Med: Rp.1.350.000',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 24,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
                child: ButtonBar(
              alignment: MainAxisAlignment.start,
            )),
          ],
        ),
      );

  Widget productDummy3() => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Ink.image(
                  image: NetworkImage(
                    'https://s1.bukalapak.com/img/17045341751/large/data.jpeg',
                  ),
                  height: 240,
                  fit: BoxFit.contain,
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  left: 16,
                  child: Text(
                    'Erka the original Stethoscope: Rp.2.000.000',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 24,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
                child: ButtonBar(
              alignment: MainAxisAlignment.start,
            )),
          ],
        ),
      );
}
