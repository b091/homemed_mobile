class Album {
  final String nama;
  final String email;

  Album({
    required this.nama,
    required this.email,
  });

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      nama: json['nama'],
      email: json['email'],
    );
  }
}
