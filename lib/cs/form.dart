// ignore_for_file: prefer_const_constructors, prefer_const_declarations, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers

import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:homemed_mobile/cs/chatpage.dart';
import 'package:homemed_mobile/cs/model.dart';
import 'package:homemed_mobile/cs/repo.dart';
import 'package:http/http.dart' as http;
import 'button_widget.dart';

class myForm extends StatefulWidget {
  @override
  _myFormState createState() => _myFormState();
}

class _myFormState extends State<myForm> {
  //Ini buat setting navbar
  final formKey = GlobalKey<FormState>();
  String nama = '';
  String email = '';

  Widget Judul() => Text(
        "Dapatkan Informasi Mengenai Produk Kami",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      );

  //Referensi : https://github.com/boriszv/Programming-Addict-Code-Examples/blob/master/flutter_forms/lib/main.dart
  @override
  Widget build(BuildContext context) => Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => ChatDetailPage()));
          },
          child: Icon(Icons.chat),
        ),
        appBar: AppBar(
          // title: Text(widget.title),
          flexibleSpace: SafeArea(
            child: Container(
              padding: const EdgeInsets.only(right: 16),
              child: Row(
                children: <Widget>[
                  // IconButton(
                  //   onPressed: () {
                  //     Navigator.pop(context);
                  //   },
                  //   // ignore: prefer_const_constructors
                  //   icon: Icon(
                  //     Icons.arrow_back,
                  //     color: Colors.black,
                  //   ),
                  // ),
                  // ignore: prefer_const_constructors
                  // SizedBox(
                  //   width: 10,
                  // ),
                  // const CircleAvatar(
                  //   maxRadius: 20,
                  // ),
                  const SizedBox(
                    width: 12,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      // children: <Widget>[],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: Form(
          key: formKey,
          child: ListView(
            padding: EdgeInsets.all(16),
            children: [
              Judul(),
              const SizedBox(height: 16),
              buildNama(),
              const SizedBox(height: 16),
              buildEmail(),
              const SizedBox(height: 32),
              // buildSubmit(),
              SizedBox(
                width: 100,
                height: 50,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    "Subscribe",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w300),
                  ),
                  color: Colors.red,
                  onPressed: () {
                    final isValid = formKey.currentState!.validate();

                    if (isValid) {
                      formKey.currentState!.save();

                      final message =
                          '$nama dengan email $email berhasil didaftarkan';
                      print(nama);
                      print(email);
                      print(message);

                      final snackBar = SnackBar(
                        content: Text(
                          message,
                          style: TextStyle(fontSize: 16),
                        ),
                        backgroundColor: Colors.green,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  },
                ),
              ),
              SizedBox(height: 100),
              // Center(
              //   child: Text(
              //     "Mereka Yang Sudah Bergabung",
              //     style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              //   ),
              // ),
              // SizedBox(height: 30),

              // Card(
              //   child: Padding(
              //       padding: const EdgeInsets.all(8.0),
              //       child: Column(
              //         children: <Widget>[
              //           Text(
              //             "Nama",
              //             style: TextStyle(
              //                 fontSize: 18, fontWeight: FontWeight.bold),
              //           )
              //         ],
              //       )),
              // )
            ],
          ),
        ),
      );

  Widget buildNama() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Nama',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          // final pattern = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
          // final regExp = RegExp(pattern);

          if (value!.isEmpty) {
            return 'Enter a name';
          }
          // else if (!regExp.hasMatch(value)) {
          //   return 'Enter a valid email';
          // }
          else {
            return null;
          }
        },
        maxLength: 50,
        onSaved: (value) => setState(() => nama = value!),
      );

  Widget buildEmail() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Email',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          final pattern = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
          final regExp = RegExp(pattern);

          if (value!.isEmpty) {
            return 'Enter an email';
          } else if (!regExp.hasMatch(value)) {
            return 'Enter a valid email';
          } else {
            return null;
          }
        },
        keyboardType: TextInputType.emailAddress,
        onSaved: (value) => setState(() => email = value!),
      );

  // Widget buildSubmit() => TextButton(
  //       child: Text("Subscribe"),
  //       onPressed: () {
  //         final isValid = formKey.currentState!.validate();

  //         if (isValid) {
  //           formKey.currentState!.save();

  //           final message = '$nama dengan email $email berhasil didaftarkan';
  //           print(nama);
  //           print(email);
  //           print(message);

  //           final snackBar = SnackBar(
  //             content: Text(
  //               message,
  //               style: TextStyle(fontSize: 300),
  //             ),
  //             backgroundColor: Colors.green,
  //           );
  //           ScaffoldMessenger.of(context).showSnackBar(snackBar);
  //         }
  //       },
  //       style: TextButton.styleFrom(
  //           primary: Colors.white, backgroundColor: Colors.red),
  //     );
}
