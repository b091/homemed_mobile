// ignore_for_file: prefer_const_constructors, prefer_const_declarations, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:homemed_mobile/cs/chatpage.dart';
import 'package:homemed_mobile/cs/form.dart';
import 'package:homemed_mobile/cs/model.dart';
import 'package:homemed_mobile/cs/repo.dart';
import 'package:homemed_mobile/homepage/home.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class cs extends StatelessWidget {
  static String title = 'Customer Support';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primarySwatch: Colors.green),
        home: csPage(title: title),
      );
}

class csPage extends StatefulWidget {
  final String title;

  const csPage({
    required this.title,
  });

  @override
  csPageState createState() => csPageState();
}

class csPageState extends State<csPage> {
  List<Album> listAlbum = [];
  Repo repository = Repo();

  getData() async {
    listAlbum = await repository.getData();
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  //Ini buat setting navbar
  int _currentIndex = 0;
  final List<Widget> _children = [
    homePage(),
    cs(),
  ];
  final formKey = GlobalKey<FormState>();
  String nama = '';
  String email = '';

  Widget Judul() => Text(
        "Dapatkan Informasi Mengenai Produk Kami",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      );

  //Referensi : https://github.com/boriszv/Programming-Addict-Code-Examples/blob/master/flutter_forms/lib/main.dart
  @override
  Widget build(BuildContext context) => Scaffold(
        floatingActionButton: SpeedDial(
          animatedIcon: AnimatedIcons.menu_close,
          children: [
            SpeedDialChild(
                child: Icon(Icons.add),
                label: "Add",
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => myForm()));
                }),
            SpeedDialChild(
                child: Icon(Icons.chat),
                label: "Chat",
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ChatDetailPage()));
                }),
            // SpeedDialChild(
            //     child: Icon(Icons.refresh),
            //     label: "Refresh",
            //     onTap: () {
            //       Navigator.of(context)
            //           .push(MaterialPageRoute(builder: (context) => cs()));
            //     }),
          ],
        ),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     Navigator.of(context).push(
        //         MaterialPageRoute(builder: (context) => ChatDetailPage()));
        //   },
        //   child: Icon(Icons.chat),
        // ),

        // bottomNavigationBar: Container(
        //   height: 60,
        //   color: Colors.black12,
        //   child: InkWell(
        //     onTap: () => cs(),
        //     child: Padding(
        //       padding: EdgeInsets.only(top: 8.0),
        //       child: Column(
        //         children: <Widget>[
        //           Icon(
        //             Icons.refresh,
        //           ),
        //           Text('Refresh'),
        //         ],
        //       ),
        //     ),
        //   ),
        // ),

        bottomNavigationBar: BottomNavigationBar(
          // type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.green[50],
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.navigate_before), label: "Back"),
            BottomNavigationBarItem(
                icon: Icon(Icons.refresh), label: "Refresh"),
            BottomNavigationBarItem(
                icon: Icon(Icons.navigate_next), label: "Next"),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),
        appBar: AppBar(
          title: Text(widget.title),
        ),

        body: ListView.separated(
            itemBuilder: (context, index) {
              return Container(
                padding: EdgeInsets.only(top: 30, bottom: 30),
                // padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                    // "Nama : " + listAlbum[index].nama,
                    listAlbum[index].nama,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider(
                thickness: 2,
                height: 10,
              );
            },
            itemCount: listAlbum.length),
      );

  Widget buildNama() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Nama',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Enter a name';
          } else {
            return null;
          }
        },
        maxLength: 50,
        onSaved: (value) => setState(() => nama = value!),
      );

  Widget buildEmail() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Email',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          final pattern = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
          final regExp = RegExp(pattern);

          if (value!.isEmpty) {
            return 'Enter an email';
          } else if (!regExp.hasMatch(value)) {
            return 'Enter a valid email';
          } else {
            return null;
          }
        },
        keyboardType: TextInputType.emailAddress,
        onSaved: (value) => setState(() => email = value!),
      );
}
