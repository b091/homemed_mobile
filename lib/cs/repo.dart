import 'dart:convert';

import 'package:homemed_mobile/cs/model.dart';
import 'package:http/http.dart' as http;

class Repo {
  final _baseUrl = "https://homemed2.herokuapp.com/CustomerSupport/json";
  // final _baseUrl = "https://homemed2.herokuapp.com/CustomerSupport/json";

  Future<List<Album>> getData() async {
    List<Album> album = [];
    try {
      final response = await http.get(Uri.parse(_baseUrl));

      if (response.statusCode == 200) {
        print(response.body);
        Iterable it = jsonDecode(response.body);
        for (var item in it) {
          Album classAlbum = new Album(
              nama: item['fields']['nama'], email: item['fields']['email']);
          album.add(classAlbum);
        }
        // album = it.map((e) => Album.fromJson(e)).toList();
      }
    } catch (e) {
      // print('CATCH');
      print(e.toString());
    }
    // print("ALBUM");
    // print(album);
    return album;
  }
}
