// ignore_for_file: non_constant_id_productentifier_names
import 'dart:convert';
import 'dart:core';


class ReviewNya {
  final String Nama;
  final String Reviewnya;
  final String id_product;

  // ReviewNya(this.Nama, this.Reviewnya, this.id_product);

  ReviewNya({
    required this.Nama,
    required this.Reviewnya,
    required this.id_product,
  });

  Map<String, dynamic> toMap(){
    return{
      'Nama' : Nama,
      'Reviewnya' : Reviewnya,
      'id_product' : id_product
    };
  }

  factory ReviewNya.fromJson(Map<String, dynamic> json) {
    return ReviewNya(
      Nama: json['Nama'],
      Reviewnya: json['Reviewnya'],
      id_product: json['id_product'],
    );
  }

  String toJson() => json.encode(toMap());

  @override
  String toString() {
    return 'Project(Nama: $Nama, Reviewnya: $Reviewnya, id_product: $id_product)';
  }

}