// ignore_for_file: deprecated_member_use

// import 'package:cbflutter/constant.dart';
import 'package:flutter/material.dart';
import 'package:homemed_mobile/detailproduct/constant.dart';
// import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Wrap(
        children: <Widget>[
          Container(
            height: size.height * 0.3,
            width: size.width * 1,
            color: kPrimaryColor,
            padding: const EdgeInsets.all(30.0),
            alignment: Alignment.center,
            child: Stack(
              children:<Widget> [
                Image.asset(
                  'assets/images/kn95fix.jpg',
                  width: 150,
                  height: 150,
                  fit: BoxFit.contain,              
                ),
              ],
            ),
          ),
          
          Container(
            height: size.height * 0.4,
            width: size.width,
            
            decoration: const BoxDecoration(
              color: kBackgroundColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(45),
                topRight: Radius.circular(45),
              ),
            ),
            padding: const EdgeInsets.all(30.0),
            child: Column(     
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(         
                  child: Row(                    
                    mainAxisAlignment: MainAxisAlignment.end,
                    children:const <Widget> [
                      Expanded(child: 
                        Text('Rp25.000',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 24,
                          color: Colors.black,
                          )
                        ),
                      )
                    ],
                  ),
                ),

                const SizedBox(height: 15),

                SizedBox(
                  width: double.maxFinite,
                  height: 30, // this is the height of TextField
                  child: RaisedButton(
                    color: const Color(0xFFB4EBE5),
                    onPressed: () {},
                    child: Row(children: const [
                      Icon(
                        Icons.shopping_cart_outlined,
                        color: Colors.black,
                        size: 20,
                       ),
                      Text(
                        'ADD TO CART',
                        style: TextStyle(
                          color: kPrimaryColor,
                          fontWeight: FontWeight.w900,
                          fontSize: 11,
                        ),
                      ),
                    ],)
                  ),
                ),


                const SizedBox(height: 15),

                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text('Masker KN95',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 35,
                        color: Colors.black,
                        )
                      ),

                      Text('Masker dengan kemampuan menyaring udara hingga 95 persen dan Kulitas yang sangat baik',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: kTextColor,
                        )
                      ),
                      
                    ],
                  ),
                ),
                
              ],
            )
          ),
        ],
      ),
    );
    
  }
}