// ignore_for_file: deprecated_member_use

// import 'package:cbflutter/constant.dart';
// import 'package:cbflutter/data.dart';
// import 'package:cbflutter/models.dart';
// import 'package:cbflutter/screens/detailpage/body.dart';
// import 'package:cbflutter/screens/detailpage/detail_page.dart';
// import 'package:cbflutter/screens/reviewpage/formReview.dart';


import 'package:homemed_mobile/detailproduct/constant.dart';
import 'package:homemed_mobile/detailproduct/screens/detailpage/detail_page.dart';
import 'package:homemed_mobile/detailproduct/screens/detailpage/body.dart';
import 'package:homemed_mobile/detailproduct/screens/reviewpage/formReview.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';

class Review extends StatelessWidget {
  
  // List<ReviewNya> listReview = [];
  // Data data = Data();

  // getData() async{
  //   print("masuk");
  //   listReview = await data.getData();
  // }

  // @override
  // void initState() {
  //   getData();
  //   super.initState();
  //   // list.add(MainPage());
  // }


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: size.height,
            width: size.width,
            decoration: const BoxDecoration(
              color: kBackgroundColor,
            ),
            padding: const EdgeInsets.all(30.0),
            child: Wrap(
              children:  [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children:  [
                      const Text('Reviews',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 35,
                          color: kTextColor,
                        )
                      ),

                      SizedBox(
                        width: double.maxFinite,
                        height: 30, // this is the height of TextField
                        child: RaisedButton(
                          color: kPrimaryColor,
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return kasi_review();
                                },
                              ),
                            );
                          },
                          child: 
                          const Text("ADD REVIEW",
                            textAlign:TextAlign.left,
                            style: TextStyle(                            
                              color: Color(0xFFB4EBE5),
                              fontWeight: FontWeight.w900,
                              fontSize: 11,                          
                            ),
                          ),
                        ),
                      ),

                      const SizedBox(height: 15),

                      // ListView.separated(
                      //   itemCount: listReview.length,
                      //   separatorBuilder: (BuildContext context, int index) {
                      //     return Divider();
                      //   },
                      //   itemBuilder: (BuildContext context, int index) {
                      //     int i = 0;

                      //     // if (listReview[index].id_Produk == 1){
                      //       return Container(
                      //       child: 
                      //         Text(listReview[index].Reviewnya + "\n" + listReview[index].Nama),
                      //       );

                      //     //   i++;
                      //     // }

                      //     // if (i == 0){
                      //     //   return const Text('No reviews yet',
                      //     //     textAlign: TextAlign.left,
                      //     //     style: TextStyle(
                      //     //       fontWeight: FontWeight.w500,
                      //     //       fontSize: 12,
                      //     //       color: kTextColor,
                      //     //     )
                      //     //   );
                      //     // }                          
                      //   },
                      // ),

                      const Text('No reviews yet',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                          color: kTextColor,
                        )
                      ),


                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    
  }

  @override
  // ignore: no_logic_in_create_state
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }
}