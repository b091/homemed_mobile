import 'dart:html';
// import 'package:cbflutter/constant.dart';
// import 'package:cbflutter/screens/detailpage/detail_page.dart';
import 'package:flutter/material.dart';
import 'package:homemed_mobile/detailproduct/constant.dart';
import 'package:homemed_mobile/detailproduct/screens/detailpage/detail_page.dart';

// class kasi_review extends StatelessWidget {

//   @override
//   Widget build(BuildContext context) => MaterialApp(
//     debugShowCheckedModeBanner: false,
//     theme: ThemeData(primaryColor: kBackgroundColor),
//     home: MainPage(),
//   );
// }

class kasi_review extends StatefulWidget {

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<kasi_review> {
  final formKey = GlobalKey<FormState>();
  String nama = '';
  String penilaian = '';

  Widget Judul() => const Text(
    "Add Review",
    textAlign: TextAlign.left,
    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w900, color: Colors.black)
  );

  //Referensi : https://github.com/boriszv/Programming-Addict-Code-Examples/blob/master/flutter_forms/lib/main.dart
  @override
  Widget build(BuildContext context) => Scaffold(
    floatingActionButton: FloatingActionButton(
      onPressed: () {},
      child: Icon(Icons.chat),
    ),
    appBar: AppBar(
        backgroundColor: kPrimaryColor,
        // leading: IconButton(icon: const Icon(Icons.arrow_back_ios_new_rounded), onPressed: () { 
        //   Navigator.push(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) {
        //       return detailPage();
        //     },
        //   ),
        // );
        //  },),
      ),
    body: Form(
      key: formKey,
      child: ListView(
        padding: EdgeInsets.all(16),
        children: [
          Judul(),
          const SizedBox(height: 30),
          buildNama(),
          const SizedBox(height: 16),
          reviewNya(),
          const SizedBox(height: 16),
          buildSubmit(),
        ],
      ),
    ),
  );

  Widget buildNama() => TextFormField(
    decoration: const InputDecoration(
      labelText: 'Nama',
      border: OutlineInputBorder(),
    ),
    maxLength: 50,
    validator: (value) {
      if (value!.isEmpty) {
        return 'Masukan Nama';
      } else {
        return null;
      }
    },
    onSaved: (value) => setState(() => nama = value!),
  );

  Widget reviewNya() => TextFormField(
    decoration: const InputDecoration(
      labelText: 'Penilaian',
      border: OutlineInputBorder(),
    ),
    validator: (value) {
      if (value!.isEmpty) {
        return 'Masukan Penilaian';
      } else {
        return null;
      }
    },
    onSaved: (value) => setState(() => penilaian = value!),
  );
  

  Widget buildSubmit() => TextButton(

    child: const Text("ADD REVIEW"),
    style: TextButton.styleFrom(
      primary: const Color(0xFFB4EBE5), 
      backgroundColor: kPrimaryColor,  
    ),
    
    onPressed: () {
      final isValid = formKey.currentState!.validate();

      if (isValid) {
        formKey.currentState!.save();

        final message = 'Dear $nama, terimakasih telah menambahkan penialian';
        print(nama);
        print(penilaian);
        print(message);

        final snackBar = SnackBar(
          content: Text(
            message,
            style: const TextStyle(
              fontSize: 20,
              color: kPrimaryColor),
          ),
          backgroundColor: Colors.teal[100],
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }

      Future.delayed(Duration(seconds: 5), () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return detailPage();
            },
          ),
        );
      });
      
    },
  );
}