import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF038376);
const kTextColor = Color(0xFF07BDAB);
const kBackgroundColor = Color(0xFFFFFFFF);

const kDefaultPads = 20;