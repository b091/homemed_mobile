// ignore_for_file: prefer_const_constructors, prefer_const_declarations, unnecessary_new, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:homemed_mobile/catalog/catalog.dart';
import 'package:homemed_mobile/checkout_page/checkout_page.dart';
import 'package:homemed_mobile/cs/custsup.dart';
import 'package:homemed_mobile/homepage/home.dart';
import 'package:homemed_mobile/companyprofile/company.dart';
import 'package:homemed_mobile/user/sign_log.dart';
import 'cs/button_widget.dart';

void main() {
  runApp(myApp());
}

class myApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BottomNavigation(), //Ini isi sama root page Dimitri
    );
  }
}

class BottomNavigation extends StatefulWidget {
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    homePage(),
    catalog(),
    CheckoutPage(),
    // isi cart Chris
    cs(),
    cp(),
    userPage(),
  ];

  void onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _children[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTappedBar,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.green[50],
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
            BottomNavigationBarItem(icon: Icon(Icons.shopping_bag_outlined), label: "Catalog"),
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart), label: "Cart"),
            BottomNavigationBarItem(
                icon: Icon(Icons.support_agent), label: "Support"),
            BottomNavigationBarItem(icon: Icon(Icons.info), label: "About"),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),

          ],
        ));
  }
}
