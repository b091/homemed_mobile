# **HomeMed**
    Tugas Kelompok B09 - Mata Kuliah Pemrograman Berbasis Platform
    Fakultas Ilmu Komputer UI, Semester Ganjil 2021/2022
---

## Link Download Aplikasi

Berikut link untuk men-_download_ aplikasi kami

**https://ristek.link/HomeMedAPK**



## Anggota Kelompok
1. Aurora Putri Kumala Bakti - 2006597323 
2. Brasel Isnaen Fachturrahman - 2006597374 
3. Christopher Moses Nathanael - 2006597361 
4. Dimitri Theofilus Wynardo N - 2006597670 
5. Galan Gahara Triguna Toding - 2006597121 
6. Muhammad Arya Adirianto - 2006597494 
7. Nathasya Shalsabilla Putri - 2006597531 

## Deskripsi Aplikasi
Di tengah hiruknya masyarakat Indonesia akibat **Pandemi Covid-19** yang semakin pesat dan membludak, muncul permasalahan baru yaitu susahnya mencari obat dan alat kesehatan. Masyarakat sudah susah untuk beraktivitas karena keterbatasan protokol kesehatan yang ditetapkan ditambah harus susah payah juga dalam mencari obat dan alat kesehatan baik untuk penanganan penyakit maupun persediaan di rumah. Akhir-akhir ini, masyarakat Indonesia juga sedang digemparkan dengan susahnya mencari oxygen di sekitarnya. Banyak sekali hal menjadi keos selama pandemi ini. Masyarakat tak terhindarkan dari godaan untuk membeli alat kesehatan yang termurah dan tercepat aksesnya yang mereka bisa dapatkan. Muncullah permasalahan baru seperti contoh penipuan terhadap tabung oxygen yang diisi dengan nitrogen dan penipuan terhadap lapisan layer masker. Untuk itu, kami hadir bertujuan meminimalisir kekacauan yang terjadi dengan cara membuat sebuah aplikasi bagi masyarakat Indonesia berbasis website.

**Aplikasi HomeMed** hadir sebagai aplikasi untuk membantu masyarakat Indonesia dalam membeli alat kesehatan atau medis yang aman dan terpercaya. Aplikasi ini bekerja sama secara langsung dengan Kementerian Kesehatan dalam mendapatkan produk yang telah disahkan oleh BSN. Dalam membangun aplikasi ini, kami bertujuan untuk memudahkan masyarakat umum dalam mencari alat kesehatan yang resmi tanpa takut akan keaslian produk. Dengan memberikan produk yang dapat diandalkan, masyarakat akan lebih percaya dengan aplikasi kami. Aplikasi kami yang berbasis e-commerce juga memberikan nilai praktis karena masyarakat tidak perlu membeli dengan cara datang langsung ke tempat, melainkan hanya dengan membuka dan memesan melalui website. 

## Modul Aplikasi

| Modul | Person in Charge |
| --- | --- |
| Landing Page | Dimitri Theofilus Wynardo N |
| Shopping Cart | Christopher Moses Nathanael |
| Product Information Management | Brasel Isnaen Fachturrahman |
| Company Profile | Galan Gahara Triguna Toding |
| Customer Support dan Interface | Muhammad Arya Adirianto |
| Detail Product (User-generated Reviews) | Aurora Putri Kumala Bakti |
| Account Page | Nathasya Shalsabilla Putri |

## Penjelasan Modul
**1. Landing Page**

Landing page halaman pada website yang akan pertama kali dilihat oleh penggunasaat pertama kali melakukan visit pada domain website kami. Pada laman ini akan memuat beberapa barang dagangan yang dapat dikunjungi, bar navigasi untuk memudahkan berpindah antarlaman, dan search bar untuk mempermudah mencari barang yang diinginkan dengan menggunakan kata kunci. 

**2. Shopping Cart**

Pada modul ini, barang-barang yang sudah ingin dibeli akan ditampung di sini sebelum menuju ke proses pembayaran dan seterusnya. Pada modul ini akan terdapat pilihan cancel barang dan tambah barang. Setelah itu proses pembayaran juga akan dilakukan pada modul ini. Pengguna yang bisa mengakses shopping cart dan membeli barang-barang yang diinginkan hanyalah pengguna yang sudah memiliki akun saja.

**3. Product Information Management**

Pada modul inilah etalasa barang-barang yang ada di toko kami diperlihatkan. Pengguna dapat memilih berbagai macam barang yang ditawarkan. Pengguna dapat menggunakan navigation bar dan search bar untuk memudahkan pencarian barang yang diinginkan. Jika sudah menemukan barang yang ingin dibeli, pengguna dapat menambahkan barang tersebut ke shopping cart, kemudian melanjutkan proses pembelian sampai selesai. Untuk dapat melakukan hal tersebut, pengguna harus sudah memiliki akun dan login terlebih dahulu

**4. Company Profile**

Pada modul ini akan berisi informasi tentang perusahaan, HomeMed. Pada modul ini juga akan terdapat media-media sosial resmi perusahaan. Selain itu pengguna juga bisa mendapatkan kontak informasi perusahaan apabila ingin menjalin kerjasama dengan perusahaan.

**5. Customer Support and Interface**

Pada modul ini pengguna dapat berkonsultasi dengan admin yang sedang bertugas. Pengguna dapat bertanya-tanya mengenai barang-barang yang ada pada toko, seperti perbedaan antara barang A dan B, dan ketersediaan barang tersebut.

**6. Detail Product** (User-generated Reviews)

Pada modul ini pengguna dapat memberikan pengalaman mereka terhadap barang yang sudah mereka beli dan juga terhadap pelayanan yang sudah diberikan perusahaan kami selama melakukan transaksi barang tersebut. Hal ini nantinya akan berguna sebagai evaluasi kinerja perusahaan. Nantinya perusahaan dapat semakin berkembang berdasarkan masukan dari para pengguna.

**7. Account Page**

Pada modul ini pengguna dapat mendaftarkan akunnya dan login ke akun yang sudah dibuatnya. Setelah pengguna login, akan tertera data-data profil pengguna, seperti nama panggilan, email, nomor telepon, dan alamat. Pengguna juga dapat melakukan log-out jika ingin mengganti akun.

## Persona

**1. Masyarakat umum yang belum memiliki akun**

Masyarakat yang ingin mengakses aplikasi kami tetapi belum memliki akun dapat melakukan aktivitas seperti membuka halaman landing page, mencari barang, dan melihat review barang.

> **Goal** : Melihat produk kesehatan yang dibutuhkan

> **Struggle** : Menemukan produk kesehatan yang sesuai dan melalui toko yang terpercaya


**2. Masyarakat umum yang sudah memiliki akun**

Saat masyarakat sudah memiliki akun, mereka dapat melakukan operasi yang sama seperti mereka yang belum memiliki akun ditambah dapat memasukkan barang yang ingin mereka beli ke dalam shopping cart atau keranjang. Mereka juga bisa memanfaatkan modul Customer Support dan Interface yaitu chat langsung dengan Customer Service ke depannya. Mereka yang sudah memiliki akun juga bisa melihat Account Page pribadi.

> **Goal** : Dapat membeli barang yang sesuai dan terjamin kualitasnya, dapat mendapatkan bantuan apabila kesulitan dalam mengoperasikan halaman HomeMed, dapat melihat profil diri yang telah dibuat.

> **Struggle** : Menemukan produk kesehatan yang sesuai dan melalui toko yang terpercaya, kesulitan ketika mengoperasikan platform baru, kebingungan ketika menemukan masalah pada proses pembelian/penjualan produk

**3. Tenaga kesehatan**

Tenaga kesehatan di sini berfungsi sebagai supplier utama dari produk yang dihadirkan. Mereka akan memiliki akses seperti masyarakat umum yang belum memiliki akun. Mereka dapat mengontrol langsung jumlah barang yang tersedia. Mereka akan bekerja sama dengan developer aplikasi untuk men-supply produk terbaru.

> **Goal** : Dapat bekerja sama untuk melakukan supply barang atau menjual barangnya di HomeMed

> **Struggle** : Kesulitan menjadi pensupply barang, kekhawatiran ketika "menitipkan" barangnya untuk dijual


**4. Pemilik toko kesehatan**

Pemilik toko kesehatan atau para pemilik apotek dapat mendapatkan supply barang untuk tokonya berdasarkan produk yang tersedia dari kami sehingga produk yang ada di tokonya terjamin keasliannya.

> **Goal** : Dapat mendapatkan supply barang yang sesuai dan terjamin kualitasnya untuk tokonya

> **Struggle** : Menemukan produk kesehatan yang dijamin kualitasnya (termasuk produk original/asli), menemukan pen-supply barang untuk toko kesehatan.
